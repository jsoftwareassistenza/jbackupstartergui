/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jbackupstarter;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.util.Hashtable;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 *
 * @author pgx71
 */
public class Main {

    
    public static Process processGui = null;
    public static EseguiJBackupGui eseGui = null;

    public static void main(String[] args) {
        // inizializzazione
        Runtime.getRuntime().addShutdownHook(new JBackupShutdown());
        

        GestoreCFG g = new GestoreCFG("jbackupstartergui.ini");
        if (!g.esisteFile()) {
            g.creaFile(new Hashtable());
            g.aggiornaChiave("IND_DOWNLOAD_FILE", "http://www.jazzgestionale.it/sito/download/JBACKUP2/jbackupgui_agg.zip");
            g.aggiornaChiave("IND_DOWNLOAD_LIB", "http://www.jazzgestionale.it/sito/download/JBACKUP2/jbackupgui_agg_lib.zip");
            g.aggiornaChiave("IND_DOWNLOAD_INFOAGG", "http://www.jazzgestionale.it/sito/download/JBACKUP2/info_jbackupgui_agg.txt");
            g.aggiornaChiave("PROXY", "N");
        }

        File faggx = new File("agggui");
        if (!faggx.exists()) {
            faggx.mkdir();
        }

        // lancia l'applicazione
        System.out.println("JBACKUP STARTER: lancio JBackupGui");
        eseGui = new EseguiJBackupGui();
        eseGui.start();
        

        while (true) {
       
            File fbackupincorso = new File("backupincorsogui.txt");
            boolean bic = (fbackupincorso.exists());
            if (!bic) {
                // controllo aggiornamenti
                boolean agg = false;
                GestoreCFG gc = new GestoreCFG("jbackupstartergui.ini");
                // dati link download
                String protfile = gc.leggiChiave("IND_DOWNLOAD_FILE");
                if (protfile.equals("")) {
                    protfile = "http://www.jazzgestionale.it/sito/download/JBACKUP2/jbackupgui_agg.zip";
                }
                String protlib = gc.leggiChiave("IND_DOWNLOAD_LIB");
                if (protlib.equals("")) {
                    protlib = "http://www.jazzgestionale.it/sito/download/JBACKUP2/jbackupgui_agg_lib.zip";
                }
                String protinfoagg = gc.leggiChiave("IND_DOWNLOAD_INFOAGG");
                if (protinfoagg.equals("")) {
                    protinfoagg = "http://www.jazzgestionale.it/sito/download/JBACKUP2/info_jbackupgui_agg.txt";
                }
                // dati proxy
                boolean usaproxy = (gc.leggiChiave("USAPROXY").equals("S"));
                String indproxy = gc.leggiChiave("INDPROXY");
                String portaproxy = gc.leggiChiave("PORTAPROXY");
                String userproxy = gc.leggiChiave("USERPROXY");
                String pwdproxy = gc.leggiChiave("PWDPROXY");
                boolean autproxy = (!userproxy.equals(""));
                Proxy proxy = null;
                if (usaproxy) {
                    proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(indproxy, estraiIntero(portaproxy)));
                }
                long lf = 0;
                long lflib = 0;
                long dimfinfoagg = 0;
                long dimlibinfoagg = 0;
                FileOutputStream fos = null;
                try {
                    boolean errdownloadinfo = false;
                    boolean errdownloadprog = false;
                    boolean errdownloadlib = false;
                    boolean download = false;

                    // controllo file info
                    boolean contenutoinfopres = false;
                    try {
                        URL urlAggInfo = new URL(protinfoagg);
                        if (usaproxy) {
                            URLConnection uc = urlAggInfo.openConnection(proxy);
                            if (autproxy) {
                                String encoded = new String(Base64Coder.encodeString(new String(userproxy + ":" + pwdproxy)));
                                uc.setRequestProperty("Proxy-Authorization", "Basic " + encoded);
                            }
                            contenutoinfopres = (uc.getContent() != null);
                        } else {
                            contenutoinfopres = (urlAggInfo.openConnection().getContent() != null);
                        }
                        if (contenutoinfopres) {
                            System.out.println("JBACKUP STARTER: Download info_jbackupgui_agg.txt");
                            try {
                                fos = new FileOutputStream("agggui" + File.separator + "info_jbackupgui_agg.txt");
                                int diminfo = 0;
                                InputStream in = null;
                                if (usaproxy) {
                                    URLConnection uc = urlAggInfo.openConnection(proxy);
                                    if (autproxy) {
                                        String encoded = new String(Base64Coder.encodeString(new String(userproxy + ":" + pwdproxy)));
                                        uc.setRequestProperty("Proxy-Authorization", "Basic " + encoded);
                                    }
                                    in = uc.getInputStream();
                                    diminfo = uc.getContentLength();
                                } else {
                                    in = urlAggInfo.openStream();
                                    //dimf = urlAggProg.openConnection().getContentLength();
                                    URLConnection uc = urlAggInfo.openConnection();
                                    //uc.setConnectTimeout(3000);
                                    diminfo = uc.getContentLength();
                                }
                                for (int i = 0; i < diminfo; i++) {
                                    byte[] b = new byte[1];
                                    in.read(b);
                                    fos.write(b);
                                }
                                fos.close();
                                in.close();
                            } catch (Exception ejdaa) {
                                errdownloadinfo = true;
                                System.out.println("JBACKUPGUI STARTER: errore su download info_jbackupgui_agg.txt");
                                try {
                                    fos.close();
                                } catch (Exception efos) {
                                }
                            }
                            if (!errdownloadinfo) {
                                GestoreCFG gi = new GestoreCFG("agggui" + File.separator + "info_jbackupgui_agg.txt");
                                if (!gi.leggiChiave("DIMPROG").equals("")) {
                                    dimfinfoagg = estraiLong(gi.leggiChiave("DIMPROG"));
                                }
                                if (!gi.leggiChiave("DIMLIB").equals("")) {
                                    dimlibinfoagg = estraiLong(gi.leggiChiave("DIMLIB"));
                                }
                            }
                        }
                    } catch (Exception einfo) {

                    }
                    if (!contenutoinfopres) {
                        System.out.println("JBACKUPGUI STARTER: info_jbackupgui_agg.txt non presente su sito");
                    } else {
                        long uaggprog = estraiLong(gc.leggiChiave("ultimo_agg_prog"));
                        System.out.println("JBACKUPGUI STARTER: aggiornamento attuale programma: " + uaggprog);
                        long uagglib = estraiLong(gc.leggiChiave("ultimo_agg_lib"));
                        System.out.println("JBACKUPGUI STARTER: aggiornamento attuale lib: " + uagglib);
                        URL urlAggProg = new URL(protfile);
                        if (usaproxy) {
                            URLConnection uc = urlAggProg.openConnection(proxy);
                            if (autproxy) {
                                String encoded = new String(Base64Coder.encodeString(new String(userproxy + ":" + pwdproxy)));
                                uc.setRequestProperty("Proxy-Authorization", "Basic " + encoded);
                            }
                            lf = uc.getLastModified();
                        } else {
                            lf = urlAggProg.openConnection().getLastModified();
                        }
                        System.out.println("JBACKUPGUI STARTER: aggiornamento programma sul sito: " + lf);
                        if (lf > uaggprog) {
                            // scarica aggiornamento programma
                            boolean contenutopres = false;
                            if (usaproxy) {
                                URLConnection uc = urlAggProg.openConnection(proxy);
                                if (autproxy) {
                                    String encoded = new String(Base64Coder.encodeString(new String(userproxy + ":" + pwdproxy)));
                                    uc.setRequestProperty("Proxy-Authorization", "Basic " + encoded);
                                }
                                contenutopres = (uc.getContent() != null);
                            } else {
                                contenutopres = (urlAggProg.openConnection().getContent() != null);
                            }
                            if (contenutopres) {
                                download = true;
                                System.out.println("JBACKUPGUI STARTER: Download aggiornamento jbackupgui_agg.zip");
                                int dimf = 0;
                                try {
                                    fos = new FileOutputStream("agggui" + File.separator + "jbackupgui_agg.zip");
                                    InputStream in = null;
                                    if (usaproxy) {
                                        URLConnection uc = urlAggProg.openConnection(proxy);
                                        if (autproxy) {
                                            String encoded = new String(Base64Coder.encodeString(new String(userproxy + ":" + pwdproxy)));
                                            uc.setRequestProperty("Proxy-Authorization", "Basic " + encoded);
                                        }
                                        in = uc.getInputStream();
                                        dimf = uc.getContentLength();
                                    } else {
                                        in = urlAggProg.openStream();
                                        //dimf = urlAggProg.openConnection().getContentLength();
                                        URLConnection uc = urlAggProg.openConnection();
                                        //uc.setConnectTimeout(3000);
                                        dimf = uc.getContentLength();
                                    }
                                    for (int i = 0; i < dimf; i++) {
                                        byte[] b = new byte[1];
                                        in.read(b);
                                        fos.write(b);
                                    }
                                    fos.close();
                                    in.close();
                                } catch (Exception ejdaa) {
                                    errdownloadprog = true;
                                    System.out.println("JBACKUPGUI STARTER: errore su download jbackupgui_agg.zip");
                                    try {
                                        fos.close();
                                    } catch (Exception efos) {
                                    }
                                }
                                if (!errdownloadprog) {
                                    System.out.println("JBACKUPGUI STARTER: Download aggiornamento jbackupgui_agg.zip OK");
                                }
                            }
                        }
                        if (!errdownloadprog) {
                            URL urlAggLib = new URL(protlib);
                            if (usaproxy) {
                                URLConnection uc = urlAggLib.openConnection(proxy);
                                if (autproxy) {
                                    String encoded = new String(Base64Coder.encodeString(new String(userproxy + ":" + pwdproxy)));
                                    uc.setRequestProperty("Proxy-Authorization", "Basic " + encoded);
                                }
                                lflib = uc.getLastModified();
                            } else {
                                lflib = urlAggLib.openConnection().getLastModified();
                            }
                            System.out.println("JBACKUPGUI STARTER: aggiornamento lib sul sito: " + lflib);
                            if (lflib > uagglib) {
                                // scarica aggiornamento lib
                                boolean contenutopres = false;
                                if (usaproxy) {
                                    URLConnection uc = urlAggLib.openConnection(proxy);
                                    if (autproxy) {
                                        String encoded = new String(Base64Coder.encodeString(new String(userproxy + ":" + pwdproxy)));
                                        uc.setRequestProperty("Proxy-Authorization", "Basic " + encoded);
                                    }
                                    contenutopres = (uc.getContent() != null);
                                } else {
                                    contenutopres = (urlAggLib.openConnection().getContent() != null);
                                }
                                if (contenutopres) {
                                    download = true;
                                    System.out.println("JBACKUPGUI STARTER: Download aggiornamento jbackupgui_agg_lib.zip...");
                                    int dimf = 0;
                                    try {
                                        fos = new FileOutputStream("agggui" + File.separator + "jbackupgui_agg_lib.zip");
                                        InputStream in = null;
                                        if (usaproxy) {
                                            URLConnection uc = urlAggLib.openConnection(proxy);
                                            if (autproxy) {
                                                String encoded = new String(Base64Coder.encodeString(new String(userproxy + ":" + pwdproxy)));
                                                uc.setRequestProperty("Proxy-Authorization", "Basic " + encoded);
                                            }
                                            in = uc.getInputStream();
                                            dimf = uc.getContentLength();
                                        } else {
                                            in = urlAggLib.openStream();
                                            //dimf = urlAggProg.openConnection().getContentLength();
                                            URLConnection uc = urlAggLib.openConnection();
                                            //uc.setConnectTimeout(3000);
                                            dimf = uc.getContentLength();
                                        }
                                        for (int i = 0; i < dimf; i++) {
                                            byte[] b = new byte[1];
                                            in.read(b);
                                            fos.write(b);
                                        }
                                        fos.close();
                                        in.close();
                                    } catch (Exception ejdaa) {
                                        errdownloadlib = true;
                                        System.out.println("JBACKUPGUI STARTER: errore su download jbackupgui_agg_lib.zip");
                                        try {
                                            fos.close();
                                        } catch (Exception efos) {
                                        }
                                    }
                                    if (!errdownloadlib) {
                                        System.out.println("JBACKUPGUI STARTER: Download aggiornamento jbackupgui_agg_lib.zip OK");
                                    }
                                }
                            }
                        }
                        if (download) {
                            if (errdownloadprog || errdownloadlib) {
                                svuotaDirAgg();
                                agg = false;
                            } else {
                                // verifica dimensioni aggiornamenti scaricati
                                boolean integritaagg = true;
                                File fx = new File("agggui" + File.separator + "jbackupgui_agg.zip");
                                if (fx.exists() && dimfinfoagg > 0 && fx.length() != dimfinfoagg) {
                                    integritaagg = false;
                                    System.out.println("JBACKUPGUI STARTER: Problema integrità aggiornamento scaricato");
                                    System.out.println("JBACKUPGUI STARTER: jbackupgui_agg.zip dim.locale:" + fx.length() + " dim.info.agg:" + dimfinfoagg);
                                }
                                File fx2 = new File("agggui" + File.separator + "jbackupgui_agg_lib.zip");
                                if (fx2.exists() && dimlibinfoagg > 0 && fx2.length() != dimlibinfoagg) {
                                    integritaagg = false;
                                    System.out.println("JBACKUPGUI STARTER: Problema integrità aggiornamento scaricato");
                                    System.out.println("JBACKUPGUI STARTER: jbackupgui_agg_lib.zip dim.locale:" + fx2.length() + " dim.info.agg:" + dimlibinfoagg);
                                }
                                if (!integritaagg) {
                                    svuotaDirAgg();
                                    agg = false;
                                } else {
                                    agg = true;
                                }
                            }
                        }
                    }
                } catch (Exception eagg) {
                    eagg.printStackTrace();
                    agg = false;
                }

                if (agg) {
                    // chiusura applicazione
                    boolean errchiusura = false;
                    System.out.println("JBACKUPGUI STARTER: chiusura applicazione in corso...");
                    try {
                        if (processGui != null) {
                            processGui.destroy();
                        }
                        
                    } catch (Exception ex) {
                        errchiusura = true;
                        ex.printStackTrace();
                    }
                    if (!errchiusura) {
                        System.out.println("JBACKUPGUI STARTER: chiusura applicazione OK");
                        // installazione aggiornamento scaricato
                        System.out.println("JBACKUPGUI STARTER: installazione aggiornamento in corso...");

                        File fagg = new File("agggui" + File.separator + "jbackupgui_agg.zip");
                        if (fagg.exists()) {
                            System.out.println("JBACKUPGUI STARTER: installazione jbackupgui_agg.zip in corso...");
                            boolean unzipfileok = false;
                            try {
                                int BUFFER = 2048;
                                BufferedOutputStream dest = null;
                                FileInputStream fis = new FileInputStream(fagg);
                                ZipInputStream zis = new ZipInputStream(new BufferedInputStream(fis));
                                ZipEntry entry;
                                while ((entry = zis.getNextEntry()) != null) {
                                    System.out.println("JBACKUPGUI STARTER: UNZIP " + entry.getName());
                                    int count;
                                    byte data[] = new byte[BUFFER];
                                    FileOutputStream fosz = new FileOutputStream(entry.getName());
                                    dest = new BufferedOutputStream(fosz, BUFFER);
                                    while ((count = zis.read(data, 0, BUFFER)) != -1) {
                                        dest.write(data, 0, count);
                                    }
                                    dest.flush();
                                    dest.close();
                                    fosz.close();
                                }
                                zis.close();
                                unzipfileok = true;
                            } catch (Exception eunzip) {
                                eunzip.printStackTrace();
                            }
                            //fagg.delete();
                            if (unzipfileok) {
                                System.out.println("JBACKUPGUI STARTER: installazione jbackupgui_agg.zip OK");
                                gc.aggiornaChiave("ultimo_agg_prog", "" + lf);
                            } else {
                                System.out.println("JBACKUPGUI STARTER: errore durante installazione jbackupgui_agg.zip");
                            }
                        }

                        File flib = new File("agggui" + File.separator + "jbackupgui_agg_lib.zip");
                        if (flib.exists()) {
                            System.out.println("JBACKUPGUI STARTER: installazione jbackupgui_agg_lib.zip in corso...");
                            boolean unziplibok = false;
                            try {
                                int BUFFER = 2048;
                                BufferedOutputStream dest = null;
                                FileInputStream fis = new FileInputStream(flib);
                                ZipInputStream zis = new ZipInputStream(new BufferedInputStream(fis));
                                ZipEntry entry;
                                
                                svuotaDirLibGui();
                                
                                while ((entry = zis.getNextEntry()) != null) {
                                    System.out.println("JBACKUPGUI STARTER: UNZIP LIB " + entry.getName());
                                    int count;
                                    byte data[] = new byte[BUFFER];
                                    FileOutputStream fosz = new FileOutputStream("libgui" + File.separator + entry.getName());
                                    dest = new BufferedOutputStream(fosz, BUFFER);
                                    while ((count = zis.read(data, 0, BUFFER)) != -1) {
                                        dest.write(data, 0, count);
                                    }
                                    dest.flush();
                                    dest.close();
                                    fosz.close();
                                }
                                zis.close();
                                unziplibok = true;
                            } catch (Exception eunzip) {
                                eunzip.printStackTrace();
                            }
                            //fagg.delete();
                            if (unziplibok) {
                                System.out.println("JBACKUPGUI STARTER: installazione jbackupgui_agg_lib.zip OK");
                                gc.aggiornaChiave("ultimo_agg_lib", "" + lflib);
                            } else {
                                System.out.println("JBACKUPGUI STARTER: errore durante installazione jbackupgui_agg_lib.zip");
                            }
                        }
                        svuotaDirAgg();

                        System.out.println("JBACKUPGUI: installazione aggiornamento TERMINATA");

                        // riavvio applicazione
                        eseGui.uscita = true;
                        try {
                            eseGui.join();
                        } catch (Exception ejoin) {
                            ejoin.printStackTrace();
                        }
                        eseGui = new EseguiJBackupGui();
                        eseGui.start();
                    } else {
                        System.out.println("JBACKUPGUI STARTER: errore su chiusura applicazione");
                    }
                }
            } else {
                System.out.println("JBACKUPGUI STARTER: backup in corso");
            }
            try {
                
                if (processGui != null) {
                    if (processGui.exitValue() == 0) {
                        System.out.println("Processo chiuso");
                        System.exit(0);
                    }
                }
            } catch (Exception e) {
                System.out.println("Processo aperto");
            }

            try {
                Thread.currentThread().sleep(10000);
//                Thread.currentThread().sleep(3600000);
            } catch (Exception esleep) {
            }
        }

    }

    public static void svuotaDirAgg() {
        File fd = new File("agggui");
        String[] lf = fd.list();
        for (int i = 0; i < lf.length; i++) {
            File fdel = new File("agggui" + File.separator + lf[i]);
            fdel.delete();

        }
    }
    
    public static void svuotaDirLibGui() {
        File fd = new File("libgui");
        String[] lf = fd.list();
        for (int i = 0; i < lf.length; i++) {
            File fdel = new File("libgui" + File.separator + lf[i]);
            fdel.delete();

        }
    }



    static class EseguiJBackupGui extends Thread {

        public boolean uscita = false;

        public EseguiJBackupGui() {
        }

        public void run() {
            try {
                String s = System.getProperties().getProperty("os.name");
                if (s.toLowerCase().startsWith("win")) {

                    FileOutputStream f = null;
                    try {
//                        f = new FileOutputStream("backuploggui.txt");
//                        f.write("start\r\n".getBytes());

                        String par_runtime = "-Xmx64m";
                        String[] cmd = new String[4];
//                    cmd[0] = ".." + File.separator + "jre" + File.separator + "bin" + File.separator + "java";
                        cmd[0] = System.getProperties().getProperty("java.home") + File.separator + "bin" + File.separator + "java";
                        cmd[1] = par_runtime;
                        cmd[2] = "-jar";
                        cmd[3] = "JazzBackup.jar";
                        System.out.println("comando :" + cmd[0] + "  " + cmd[1] + " " + cmd[2] + " " + cmd[3]);
                        ProcessBuilder pb = new ProcessBuilder(cmd);
                        pb = pb.redirectErrorStream(true);
                        processGui = pb.start();
                        InputStream is = processGui.getInputStream();
                        InputStreamReader isr = new InputStreamReader(is);
                        BufferedReader br = new BufferedReader(isr);
                        String line;
                        while ((line = br.readLine()) != null && !uscita) {
                            System.out.println(line);
                        }
//                        f.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

//    private static boolean eseguiJBackup() {
//        boolean ris = false;
//        try {
//            String s = System.getProperties().getProperty("os.name");
//            if (s.toLowerCase().startsWith("win")) {
//                String par_runtime = "";
//                String[] cmd = new String[4];
//                cmd[0] = "java";
//                cmd[1] = par_runtime;
//                cmd[2] = "-jar";
//                cmd[3] = "JazzBackup.jar";
//                System.out.println("comando :" + cmd[0] + "  " + cmd[1] + " " + cmd[2] + " " + cmd[3]);
//                ProcessBuilder pb = new ProcessBuilder(cmd);
//                pb = pb.redirectErrorStream(true);
//                process = pb.start();
//                InputStream is = process.getInputStream();
//                InputStreamReader isr = new InputStreamReader(is);
//                BufferedReader br = new BufferedReader(isr);
//                String line;
//                while ((line = br.readLine()) != null) {
//                    System.out.println(line);
//                }
//                ris = true;
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return ris;
//    }
    public static boolean copiaFile(String fOrig, String fDest) {
        byte[] b = new byte[8192];
        FileInputStream fis = null;
        FileOutputStream fos = null;
        boolean ok = true;
        int nBlocchi, nScarto;
        boolean scarto;
        int i;

        // controllo esistenza file di origine
        File fo = new File(fOrig);
        if (!fo.exists()) {
            return false;
        }
        nBlocchi = (int) fo.length() / 8192;
        scarto = ((fo.length() % 8192) != 0);
        nScarto = (int) (fo.length() % 8192);
        // copia effettiva
        try {
            fis = new FileInputStream(fOrig);
            fos = new FileOutputStream(fDest);
            for (i = 0; i < nBlocchi; i++) {
                fis.read(b);
                fos.write(b);
            }
            // copia blocco di scarto
            if (scarto) {
                byte[] sc = new byte[nScarto];
                fis.read(sc);
                fos.write(sc);
            }
            fis.close();
            fos.close();
        } catch (Exception e) {
            ok = false;
            e.printStackTrace();
        } finally {
            try {
                fis.close();
                fos.close();
            } catch (IOException e) {
                ok = false;
            }
            return ok;
        }
    }

    public static long estraiLong(String s) {
        String ris = "";
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '+'
                    || s.charAt(i) == '-'
                    || Character.getType(s.charAt(i))
                    == Character.DECIMAL_DIGIT_NUMBER) {
                ris = ris + s.charAt(i);
            }
        }
        try {
            return (new Long(ris)).longValue();
        } catch (Exception e) {
            return 0;
        }
    }

    public static int estraiIntero(String s) {
        String ris = "";
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '+'
                    || s.charAt(i) == '-'
                    || Character.getType(s.charAt(i))
                    == Character.DECIMAL_DIGIT_NUMBER) {
                ris = ris + s.charAt(i);
            }
        }
        if (ris.length() > 1) {
            if (ris.substring(ris.length() - 1).equals("-")) {
                ris = ris.substring(ris.length() - 1) + ris.substring(0, ris.length() - 1);
            } else if (ris.substring(ris.length() - 1).equals("+")) {
                ris = ris.substring(0, ris.length() - 1);
            }
        }
        try {
            return (new Integer(ris)).intValue();
        } catch (Exception e) {
            return 0;

        }
    }

    static class JBackupShutdown extends Thread {

        public void run() {
            try {
                processGui.destroy();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
